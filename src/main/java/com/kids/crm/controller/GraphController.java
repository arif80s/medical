package com.kids.crm.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GraphController
{
    @RequestMapping("/graph")
    public String index(@RequestParam(value = "name") String name)
    {
        return "Hi I am "+ name + " Inside graphController";
    }

}
